package perlog

import (
	"fmt"
	"io"
	"os"
	"time"
)

type Logger interface {
	Format(level, format string, a ...interface{}) Item
	Normal(level, message string) Item
	SetFormatter(formatter Formatter)
	SetWriter(writer io.Writer)
}

func NewLogger(level string) Logger {
	tf := TextFormatter(TextFormatterConfig{
		ConcatData: false,
		Sort: true,
	})
	return &logger{
		Writer:    os.Stdout,
		LogLevel:  LevelValue(level),
		Formatter: tf,
	}
}

type logger struct {
	Writer    io.Writer
	LogLevel  Level
	Formatter Formatter
}

func (l *logger) ok(level Level) bool {
	return level <= l.LogLevel
}

func (l *logger) Format(lvl, format string, a ...interface{}) Item {
	level := LevelValue(lvl)
	return &item{
		data:      make(Arguments, 5),
		t:         time.Now(),
		lvl:       level,
		msg:       fmt.Sprintf(format, a),
		ok:        l.ok(level),
		writer:    l.Writer,
		formatter: l.Formatter,
	}
}

func (l *logger) Normal(lvl, message string) Item {
	level := LevelValue(lvl)
	return &item{
		data:      make(Arguments, 5),
		t:         time.Now(),
		lvl:       level,
		msg:       message,
		ok:        l.ok(level),
		writer:    l.Writer,
		formatter: l.Formatter,
	}
}

func (l *logger) SetFormatter(formatter Formatter) {
	l.Formatter = formatter
}

func (l *logger) SetWriter(writer io.Writer) {
	l.Writer = writer
}
