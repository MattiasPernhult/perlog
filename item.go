package perlog

import (
	"fmt"
	"io"
	"time"
)

type Item interface {
	Add(key string, value interface{}) Item
	With(arguments Arguments) Item
	Print()
}

type item struct {
	data      Arguments
	t         time.Time
	lvl       Level
	msg       string
	ok        bool
	writer    io.Writer
	formatter Formatter
}

func (i *item) Add(key string, value interface{}) Item {
	i.data[key] = value
	return i
}

func (i *item) With(arguments Arguments) Item {
	for k, v := range arguments {
		i.data[k] = v
	}
	return i
}

func (i *item) Print() {
	if i.ok {
		fmt.Fprint(i.writer, string(i.formatter.Format(i)))
	}
}
