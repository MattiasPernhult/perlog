package perlog

import "strings"

const (
	DebugLevel Level = 4
	InfoLevel  Level = 3
	ErrorLevel Level = 2
	FatalLevel Level = 1
)

type Arguments map[string]interface{}

type Level uint16

func (l Level) String() string {
	switch l {
	case DebugLevel:
		return "debug"
	case InfoLevel:
		return "info"
	case ErrorLevel:
		return "error"
	case FatalLevel:
		return "fatal"
	}

	return "all"
}

func LevelValue(value string) Level {
	switch strings.ToLower(value) {
	case "debug":
		return DebugLevel
	case "info":
		return InfoLevel
	case "error":
		return ErrorLevel
	case "fatal":
		return FatalLevel
	}

	return 0
}
