package perlog

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os"
	"sort"
	"strings"
	"time"
)

const (
	red    = 31
	green  = 32
	yellow = 33
	blue   = 36
	gray   = 37
)

type Formatter interface {
	Format(*item) []byte
}

type textFormatter struct{
	ConcatData bool
	Sort bool
}

type TextFormatterConfig struct {
	ConcatData bool
	Sort bool
}

func TextFormatter(config TextFormatterConfig) Formatter {
	return &textFormatter{
		ConcatData: config.ConcatData,
		Sort: config.Sort,
	}
}

func (f *textFormatter) Format(i *item) []byte {
	b := &bytes.Buffer{}
	keys := make([]string, 0, len(i.data))
	for k := range i.data {
		keys = append(keys, k)
	}

	if f.Sort {
		sort.Strings(keys)
	}

	t, ok := i.writer.(*os.File)
	if ok && t == os.Stdout {
		f.colorPrint(b, i, keys, f.colorLevel(i.lvl))
	} else {
		f.appendKeyValue(b, "level", i.lvl.String())
		if i.msg != "" {
			f.appendKeyValue(b, "msg", i.msg)
		}
		if !f.ConcatData {
			b.WriteByte('\n')
		}
		for _, key := range keys {
			f.appendKeyValue(b, key, i.data[key])
		}
	}

	b.WriteByte('\n')
	b.WriteByte('\n')
	return b.Bytes()
}

func (f *textFormatter) colorPrint(b *bytes.Buffer, i *item, keys []string, colorLevel int) {
	levelText := strings.ToUpper(i.lvl.String())[0:len(i.lvl.String())]

	fmt.Fprintf(b, "\x1b[%dm%s\x1b[0m[%s] %s ", colorLevel, levelText, i.t.Format(time.RFC3339), i.msg)
	for _, k := range keys {
		v := i.data[k]
		if f.ConcatData {
			fmt.Fprintf(b, " \x1b[%dm%s\x1b[0m=", colorLevel, k)
		} else {
			fmt.Fprintf(b, "\n\x1b[%dm%s\x1b[0m=", colorLevel, k)
		}
		f.appendValue(b, v)
	}
}

func (f *textFormatter) appendKeyValue(b *bytes.Buffer, key string, value interface{}) {
	b.WriteString(key)
	b.WriteByte('=')
	f.appendValue(b, value)
	if f.ConcatData {
		b.WriteByte(' ')
	} else {
		b.WriteByte('\n')
	}
}

func (f *textFormatter) appendValue(b *bytes.Buffer, value interface{}) {
	switch value := value.(type) {
	case string:
		if !f.needsQuoting(value) {
			b.WriteString(value)
		} else {
			fmt.Fprintf(b, "%q", value)
		}
	case error:
		errmsg := value.Error()
		if !f.needsQuoting(errmsg) {
			b.WriteString(errmsg)
		} else {
			fmt.Fprintf(b, "%q", errmsg)
		}
	default:
		fmt.Fprint(b, value)
	}
}

func (f *textFormatter) needsQuoting(text string) bool {
	for _, ch := range text {
		if !((ch >= 'a' && ch <= 'z') ||
			(ch >= 'A' && ch <= 'Z') ||
			(ch >= '0' && ch <= '9') ||
			ch == '-' || ch == '.') {
			return true
		}
	}
	return false
}

func (f *textFormatter) colorLevel(level Level) int {
	switch level {
	case DebugLevel:
		return gray
	case InfoLevel:
		return blue
	case ErrorLevel:
		return yellow
	case FatalLevel:
		return red
	}

	return green
}

type jsonFormatter struct{}

func JSONFormatter() Formatter {
	return &jsonFormatter{}
}

func (f *jsonFormatter) Format(i *item) []byte {
	data := map[string]interface{}{
		"arguments.time":    i.t.Format(time.RFC3339),
		"arguments.level":   i.lvl.String(),
		"arguments.message": i.msg,
	}
	for k, v := range i.data {
		switch v := v.(type) {
		case error:
			data["arguments.data."+k] = v.Error()
		default:
			data["arguments.data."+k] = v
		}
	}

	b, _ := json.Marshal(data)
	return append(b, '\n', '\n')
}
